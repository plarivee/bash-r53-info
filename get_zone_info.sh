#!/bin/bash


# Get Information about a zone / It's geo routing, A records and Healthchecks
# Exemple:
# Zone asked: "thiszone.example.org" , zone found: thiszone.example.org => ID Z0614618H0DFDFALD69
#     Getting Records from AWS Route53.....
# supports auto login if using saml2aws client.
#
#  This is given wihtout any code quality nor try catch, It might even wipe your drive for all I know.
#
#


AWSCLI=$(which aws)
if [ -z "$AWSCLI" ]; then
	echo "aws client not found."
	exit 1
fi

JQ=$(which jq)
if [ -z "$JQ" ]; then
	echo "jq not found."
	exit 1
fi

GET_ZONES=false
GET_HEALTHCHECKS=false
AUTO_LOGON=false
BIND_ZONE=false

# Colors!
COLRST="\033[0m"
ERROR_COLOR="\033[0;31m"
OK_COLOR="\033[0;32m"

if ! options=$(getopt -u -o H,a,p:,z: -l healthchecks,autologin,profile:,zone:,bind -- "$@")
then
    exit 1
fi

set -- $options

while [ $# -gt 0 ]
do
    case $1 in
    -p|--profile) 
      AWS_PROFILE=$2 ; shift;;
    -H|--healthchecks) 
      GET_HEALTHCHECKS="true" ;;
    -a|--autologin) 
      AUTO_LOGON="true" ;;
    --bind) 
      BIND_ZONE="true" ;;
    -z|--zone) 
      ZONE_DOMAIN="$2" ; shift;;
    (--) 
      shift; break;;
    (-*) 
      echo "$0: error - unrecognized option $1" 1>&2; exit 1;;
    (*) 
      break;;
    esac

    shift
done


if [ -z "$AWS_PROFILE" ]; then
	echo "$0 -p aws_profile_name [ -z zone_domain_name] [ -H (gets healthchecks)] [--bind ]"
	echo "-p : aws profile mandatory"
	echo "--bind: exports the zone in bind style of all records"
	echo "This is given wihtout any code quality nor try catch, It might even wipe your drive for all I know."
	exit 1	
fi

if [ -z "$ZONE_DOMAIN" ]; then
	GET_ZONES=true
fi

if [ "$AUTO_LOGON" = "true" ] && [ -z "$(which saml2aws)" ]; then
	echo "Auto login feature only supports saml2aws. saml2aws not found."
	exit 1

fi

###### functions #########

function check_if_logged_on() {
	# test connection
	AWS_TEST=$($AWSCLI --profile $AWS_PROFILE sts get-caller-identity)
	exit_code=$?
	if [ $exit_code -ne 0 ]; then
		echo "$AWS_TEST - You might not be logged on"
		if [ $AUTO_LOGON = "true" ]; then
			saml2aws login
		else	
			exit 1
		fi	
	else
		echo "AWS session found, $(echo ${AWS_TEST} | jq '.Arn' --raw-output)"
	fi
}

function output_records() {

	ZONE_RECORDS_JSON=$(get_all_records $1)
	ZONE_GEO_RECORDS=$(echo $ZONE_RECORDS_JSON |  jq  '.ResourceRecordSets[] | select(.Type=="A") | select(.GeoLocation!=null) | "\(.Name) \(.GeoLocation.ContinentCode) \(.AliasTarget.DNSName)"' --raw-output)
	ZONE_A_RECORDS=$(echo $ZONE_RECORDS_JSON |  jq  '.ResourceRecordSets[] | select(.Type=="A") | select(.GeoLocation==null) | "\t\(.Name | sub("[.]$"; ""))\t\t\(.ResourceRecords[].Value)\tWeight: \(.Weight // empty)"' --raw-output)

	if [ -z "$ZONE_GEO_RECORDS" ]; then
		printf "\n${ERROR_COLOR}NO GEO RECORDS FOUND IN THIS ZONE $COLRST\n"
	else
		printf "\n\033[0;35mGEO RECORDS $COLRST\n"
		# Read each line, corresponding to each GEO locations that are set
		while IFS= read -r i; do
			GEO_LOCATION=$(echo $i | awk {'print $2'})
			if [ $GEO_LOCATION = "null" ]; then
				GEO_LOCATION="DEFAULT"
			fi
			GEO_DNS=$(echo $i | awk {'print $3'})

			printf "${OK_COLOR}$GEO_LOCATION => $GEO_DNS $COLRST\n"
			# For each GEO location, get the records associated to that GEO pointer and print their values
			echo $ZONE_RECORDS_JSON |  jq  '.ResourceRecordSets[] | select(.Type=="A") | select(.Name=='\"$GEO_DNS\"') | "\t\(.Name) : \(.ResourceRecords[].Value)\tWeight: \(.Weight)"' --raw-output
			echo ""
		done <<< "$ZONE_GEO_RECORDS"
	fi

	if [ -z "$ZONE_A_RECORDS" ]; then
		printf "${ERROR_COLOR}NO A RECORDS FOUND IN THIS ZONE ${COLRST}\n"
	else
		printf "\033[0;35mA RECORDS\033[0m\n"
		printf '%s\n' "$ZONE_A_RECORDS"
		
	fi	

}

function get_all_records() {
	$AWSCLI --profile $AWS_PROFILE --no-paginate route53  list-resource-record-sets --hosted-zone-id $1
}

function get_zones() {
	printf "\033[0;32m"
	printf '%s\n' "$($AWSCLI --profile $AWS_PROFILE --no-paginate route53 list-hosted-zones | jq '.HostedZones[] | "\t\(.Name | sub("[.]$"; ""))"' --raw-output)"
}

function get_healhcheck_status() {
	# list-resource-record-sets --hosted-zone-id Z00766713NV6K4RVUIAEI | jq '.ResourceRecordSets[] | .HealthCheckId // empty' --raw-output | sort -u
	# Get the healthchecks id
	ZONE_HC_IDS=$(echo $ZONE_RECORDS_JSON | jq '.ResourceRecordSets[] | .HealthCheckId // empty' --raw-output | sort -u)
	printf "\n\033[0;35mHEALTH CHECKS\033[0m\n"
	HC_COUNT=$(wc -l <<< $ZONE_HC_IDS)
	if [ $GET_HEALTHCHECKS = "false" ]; then
		echo "Not fetching the $HC_COUNT healthchecks as asked"
	else

		echo "$HC_COUNT Healthchecks to fetch...." 
		while IFS= read -r h; do
			HC_INFO=""
			HC_STATUS=""
			#Get HC infoget-health-check --health-check-id
			HC_INFO=$($AWSCLI --profile $AWS_PROFILE --no-paginate route53 get-health-check --health-check-id $h | jq '.HealthCheck.HealthCheckConfig | "\t FQDN: \(.FullyQualifiedDomainName) \(.IPAddress) \(.Type) \(.Port)"' --raw-output)
			printf '%s\n' "$h $HC_INFO"

			# Get the status of the probes for this Healthcheck
			echo -e '\033[0;32m\tSTATUS\033[0m'
			HC_STATUS=$($AWSCLI --profile $AWS_PROFILE --no-paginate route53 get-health-check-status --health-check-id $h | jq '.HealthCheckObservations[] | "\t\(.Region)\t\t\(.StatusReport.Status | if test(".*Failure.*") then "\u001b[0;31m\(.)\u001b[0m" else "\u001b[32;1m\(.)\u001b[0m" end)"' --raw-output)
			printf '%s\n' "$HC_STATUS"
			echo ""
		done <<< $ZONE_HC_IDS
	fi
}


# MAIN

## Make sure we are logged in and have a valid token
check_if_logged_on

# Get the list of zones in that account/region
if [ $GET_ZONES == "true" ]; then
	get_zones
	exit 0
fi

# Proceed to get zone info passed on the cli arg
# Get zone info with a search with the dns-name , limit search to 1 result
ZONE_LIST_JSON=$($AWSCLI --profile $AWS_PROFILE --no-paginate route53 list-hosted-zones-by-name --dns-name $ZONE_DOMAIN --max-items 1)

if [ -z "$ZONE_LIST_JSON" ]; then
	exit 1
fi

ZONE_NAME_RETURNED=$( echo $ZONE_LIST_JSON | $JQ '.HostedZones[0] | .Name | sub("[.]$"; "")' --raw-output )
ZONE_ID_RETURNED=$( echo $ZONE_LIST_JSON | $JQ '.HostedZones[0] | .Id' --raw-output  | awk -F / {'print $3'} )

# Verify the search found matches the name we want and proceed to get the records.
if [ $ZONE_DOMAIN = $ZONE_NAME_RETURNED ]; then
	printf "\033[0;32mZone asked: \"${ZONE_DOMAIN}\" , found zone: $ZONE_NAME_RETURNED => ID $ZONE_ID_RETURNED ${COLRST}\n"

	if [ $BIND_ZONE == "true" ]; then
		printf "\033[0;34m    Getting Records from AWS Route53 to output as bind style zone \033[0m\n"
		get_all_records $ZONE_ID_RETURNED | jq -jr '.ResourceRecordSets[] | "\(.Name) \t\(.TTL) \t\(.Type) \t\(.ResourceRecords[]?.Value)\n"'
	else
		printf "\033[0;34m    Getting Records from AWS Route53.....\033[0m\n"
		output_records $ZONE_ID_RETURNED 
		get_healhcheck_status
	fi
else 
	printf "${ERROR_COLOR}Zone asked: \"${ZONE_DOMAIN}\" , zone found does not match: $ZONE_NAME_RETURNED ${COLRST}\n"
	echo ${ZONE_LIST_JSON} | jq .
fi
